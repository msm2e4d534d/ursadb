use std::char;

use core::{Query, Command};

use nom::{IResult, multispace};

enum Operation {
    And,
    Or,
}

named!(hex_char< &[u8] >, alt!(
    tag!("0") | tag!("1") | tag!("2") | tag!("3") | tag!("4") |
    tag!("5") | tag!("6") | tag!("7") | tag!("8") | tag!("9") |
    tag!("a") | tag!("b") | tag!("c") | tag!("d") | tag!("e") | tag!("f") |
    tag!("A") | tag!("B") | tag!("C") | tag!("D") | tag!("E") | tag!("F")
));

named!(delimited_string< &[u8] >, delimited!(
    tag!("'"),
    map!(take_until!("'"), |s| s),
    tag!("'")
));

named!(string_literal< Query >, delimited!(
    tag!("'"),
    map!(take_until!("'"), |s| Query::Exact(s.to_vec())),
    tag!("'")
));

fn parse_hexbyte(b0: u8, b1: u8) -> u8 {
    let first_byte = char::from(b0).to_digit(16).unwrap();
    let second_byte = char::from(b1).to_digit(16).unwrap();
    ((first_byte << 4) | second_byte) as u8
}

named!(hexbyte< u8 >, do_parse!(
    b0: hex_char >>
    b1: hex_char >>
    (parse_hexbyte(b0[0], b1[0]))
));

named!(hex_literal< Query >, delimited!(
    delimited!(take!(0), tag!("{"), opt!(multispace)),
    map!(many1!(do_parse!(
        b: hexbyte >>
        opt!(multispace) >>
        (b)
    )), |s| Query::Exact(s.to_vec())),
    delimited!(opt!(multispace), tag!("}"), take!(0))
));

named!(parens< Query >, delimited!(
    tag!("("),
    expression,
    tag!(")")
));

named!(atom< Query >, alt!(
    string_literal |
    parens |
    hex_literal
));

named!(expression< Query >, do_parse!(
    opt!(multispace) >> initial: atom >> opt!(multispace) >>
    remainder: many0!(
        alt!(
            do_parse!(alt!(tag!("&") | tag!("and")) >> opt!(multispace) >> and: atom >> opt!(multispace) >> (Operation::And, and)) |
            do_parse!(alt!(tag!("|") | tag!("or")) >> opt!(multispace) >> or: atom >> opt!(multispace) >> (Operation::Or, or))
        )
    ) >>
    (fold_exprs(initial, remainder))
));

named!(query< Command >, do_parse!(
    tag!("select") >> multispace >>
    q: expression >>
    opt!(multispace) >> tag!(";") >>
    (Command::DoQuery(q))
));

named!(index< Command >, do_parse!(
    tag!("index") >> multispace >>
    q: delimited_string >>
    opt!(multispace) >> tag!(";") >>
    (Command::DoIndex(String::from_utf8(Vec::from(q)).unwrap())) // fix
));

named!(reindex< Command >, do_parse!(
    tag!("reindex") >> opt!(multispace) >>
    tag!(";") >>
    (Command::DoReindex()) // fix
));

named!(compact< Command >, do_parse!(
    tag!("compact") >> opt!(multispace) >>
    tag!(";") >>
    (Command::DoCompact())
));

named!(ping< Command >, do_parse!(
    tag!("ping") >> opt!(multispace) >>
    tag!(";") >>
    (Command::DoPing())
));

named!(command< Command >, alt!(
    query |
    index |
    reindex |
    compact |
    ping
));

fn fold_exprs(initial: Query, remainder: Vec<(Operation, Query)>) -> Query {
    remainder.into_iter().fold(initial, |acc, pair| {
        let (oper, expr) = pair;
        match oper {
            Operation::And => Query::Intersection(vec![acc, expr]),
            Operation::Or => Query::Union(vec![acc, expr]),
        }
    })
}

fn simplify_query(query: &Query) -> Query {
    match query {
        &Query::Exact(ref t) => Query::Exact(t.clone()),
        &Query::Intersection(ref exprs) => {
            let mut new_exprs: Vec<Query> = vec![];
            for expr in exprs.iter().map(|e| simplify_query(e)) {
                if let Query::Intersection(inner) = expr {
                    new_exprs.extend(inner)
                } else {
                    new_exprs.push(expr)
                }
            }
            Query::Intersection(new_exprs)
        },
        &Query::Union(ref exprs) => {
            let mut new_exprs: Vec<Query> = vec![];
            for expr in exprs.iter().map(|e| simplify_query(e)) {
                if let Query::Union(inner) = expr {
                    new_exprs.extend(inner)
                } else {
                    new_exprs.push(expr)
                }
            }
            Query::Union(new_exprs)
        },
    }
}

pub fn parse_query(query_data: &[u8]) -> Option<Command>{
    match command(query_data) {
        IResult::Done(_, data) => {
            let cmd = match data {
                Command::DoQuery(q) => Command::DoQuery(simplify_query(&q)),
                Command::DoIndex(x) => Command::DoIndex(x),
                Command::DoReindex() => Command::DoReindex(),
                Command::DoCompact() => Command::DoCompact(),
                Command::DoPing() => Command::DoPing(),
            };
            Option::Some(cmd)
        },
        IResult::Error(_) => Option::None,
        IResult::Incomplete(_) => Option::None,
    }
}
