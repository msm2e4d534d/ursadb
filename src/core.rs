pub type TriGram = u32;
pub type FileId = u32;

#[derive(Debug, Clone)]
pub enum Command {
    DoQuery(Query),
    DoIndex(String),
    DoReindex(),
    DoCompact(),
    DoPing(),
}

#[derive(Debug, Clone)]
pub enum Query {
    Exact(Vec<u8>),
    Intersection(Vec<Query>),
    Union(Vec<Query>),
}

#[derive(Debug, Clone)]
pub enum QueryPlan {
    Primitive(TriGram),
    Intersection(Vec<QueryPlan>),
    Union(Vec<QueryPlan>),
}

pub struct TriGramIter<'a> {
    offset: usize,
    bytes: &'a [u8],
}

impl<'a> Iterator for TriGramIter<'a> {
    type Item=TriGram;

    fn next(&mut self) -> Option<TriGram> {
        if self.offset + 3 <= self.bytes.len() {
            self.offset += 1;
            Some(
                ((self.bytes[self.offset-1] as u32) << 16) +
                ((self.bytes[self.offset+0] as u32) << 8) + 
                ((self.bytes[self.offset+1] as u32) << 0)
            )
        } else {
            None
        }
    }
}

pub struct B64Iter<'a> {
    offset: usize,
    bytes: &'a [u8],
}

impl<'a> Iterator for B64Iter<'a> {
    type Item=TriGram;

    fn next(&mut self) -> Option<TriGram> {
        while self.offset + 4 <= self.bytes.len() {
            let t0 = b64_token_to_int(self.bytes[self.offset+0] as i32);
            let t1 = b64_token_to_int(self.bytes[self.offset+1] as i32);
            let t2 = b64_token_to_int(self.bytes[self.offset+2] as i32);
            let t3 = b64_token_to_int(self.bytes[self.offset+3] as i32);
            self.offset += 1;

            if t0 >= 0 && t1 >= 0 && t2 >= 0 && t3 >= 0 {
                return Some(
                    ((t0 as u32) << 18) +
                    ((t1 as u32) << 12) +
                    ((t2 as u32) << 06) +
                    ((t3 as u32) << 00)
                )
            }
        }
        None
    }
}

fn get_trigrams_greedy(bytes: &[u8]) -> Vec<u32> {
    let mut trigrams = vec![];

    if bytes.len() < 3 {
        return trigrams
    }

    for offset in 0..(bytes.len()-2) {
        trigrams.push(
            ((bytes[offset+0] as u32) << 16) +
            ((bytes[offset+1] as u32) << 8) + 
            ((bytes[offset+2] as u32) << 0)
        )
    }

    trigrams
}

fn b64_token_to_int(chr: i32) -> i32 {
    if chr >= 0x41 && chr <= 0x5A {
        chr - 0x41
    } else if chr >= 0x61 && chr <= 0x7A {
        chr - 0x61 + 0x1A
    } else if chr >= 0x30 && chr <= 0x39 {
        chr - 0x30 + 0x34
    } else if chr == (' ' as i32) {
        0x3e
    } else if chr == ('\n' as i32) {
        0x3f
    } else {
        -1
    }
}

fn get_b64_tokens_greedy(bytes: &[u8]) -> Vec<u32> {
    let mut tokens = vec![];

    if bytes.len() < 4 {
        return tokens
    }

    for offset in 0..(bytes.len()-3) {
        let t0 = b64_token_to_int(bytes[offset+0] as i32);
        let t1 = b64_token_to_int(bytes[offset+1] as i32);
        let t2 = b64_token_to_int(bytes[offset+2] as i32);
        let t3 = b64_token_to_int(bytes[offset+3] as i32);

        if t0 >= 0 && t1 >= 0 && t2 >= 0 && t3 >= 0 {
            tokens.push(
                ((t0 as u32) << 18) +
                ((t1 as u32) << 12) + 
                ((t2 as u32) << 06) +
                ((t3 as u32) << 00)
            )
        }
    }

    tokens
}

pub fn get_trigrams<'a>(bytes: &'a [u8]) -> Box<Iterator<Item = TriGram> + 'a> {
    Box::new(TriGramIter {
        offset: 0,
        bytes: bytes,
    })
}

pub fn get_b64_tokens<'a>(bytes: &'a [u8]) -> Box<Iterator<Item = TriGram> + 'a> {
    Box::new(B64Iter {
        offset: 0,
        bytes: bytes,
    })
}
