#[macro_use]
extern crate nom;
extern crate walkdir;
extern crate byteorder;
extern crate memmap;
extern crate clap;
extern crate indicatif;
extern crate rustc_serialize;

use indicatif::{ProgressBar, ProgressStyle};
use clap::{Arg, App, SubCommand, AppSettings};
use walkdir::WalkDir;
use byteorder::{ReadBytesExt, WriteBytesExt, LittleEndian};
use memmap::{Mmap, Protection};

use std::error::Error;
use std::iter::FromIterator;
use std::fs::{File, remove_file, create_dir};
use std::path::{Path, PathBuf};
use std::io::{Cursor, BufWriter, SeekFrom, BufReader, self};
use std::io::prelude::*;
use std::collections::BTreeMap;
use std::net::{TcpListener, TcpStream};

use rustc_serialize::json;
use parse::{parse_query};
use core::{Command, Query, QueryPlan, TriGram, FileId, get_trigrams, get_b64_tokens};

mod parse;
mod core;

use std::fmt;

#[derive(Debug)]
struct RuntimeError {
    message: String,
}

impl RuntimeError {
    fn new(message: &str) -> RuntimeError {
        RuntimeError {
            message: message.to_owned(),
        }
    }
}

impl Error for RuntimeError {
    fn description(&self) -> &str {
        self.message.as_str()
    }
}

impl fmt::Display for RuntimeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.description())
    }
}

enum PartialResult {
    Everything,
    Selection(Vec<FileId>),
}

struct OnDiskDatabase {
    allocated_datasets: u32,
    name: String,
    dirname: PathBuf,
    datasets: Vec<OnDiskDataset>,
}

struct OnDiskDataset {
    elements: Vec<DataIndex>,
    filename: String,
    files_filename: String,
    files: Vec<String>,
}

trait Database {
    fn file_count(&self) -> usize;
    fn name(&self, FileId) -> &Path;
}

struct DataIndex {
    filename: String,
    ntype: IndexType,
    mmap: memmap::Mmap,
    run_offsets: Vec<usize>,
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
enum IndexType {
    NGram3 = 0,
    Base64 = 1,
}

fn read_compressed_run(run: &[u8]) -> Vec<FileId>{
    let mut cursor_base = Cursor::new(run);
    let cursor = cursor_base.by_ref();

    let mut result = vec![];
    let mut acc = 0 as u32;
    let mut shift = 0;
    let mut base = 0;
    while cursor.position() < run.len() as u64 {
        // this should always succeed, and if not, panic is in order
        let next = cursor.read_u8().unwrap() as u32;
        acc += (next & 0x7F) << shift;
        shift += 7;
        if (next & 0x80) == 0 {
            base += acc;
            result.push(base - 1);
            acc = 0;
            shift = 0;
        }
    }

    result
}

impl OnDiskDataset {
    fn get_file_name(&self, id: u32) -> &String {
        &self.files[id as usize]
    }
}

impl OnDiskDatabase {
    fn allocate_dataset(&mut self, types: &Vec<IndexType>) -> DatasetBuilder {
        let mut indexes = vec![];
        
        for ndxtype in types {
            indexes.push(IndexData::new(*ndxtype));
        }

        let setid = self.allocated_datasets;
        self.allocated_datasets += 1;

        DatasetBuilder {
            indexed_bytes: 0,
            setid: setid,
            fileid: 0,
            indexes: indexes,
            index_refs: vec![],
            files: vec![],
            dirname: self.dirname.clone(),
        }
    }

    fn full_name(&self, filename: &str) -> PathBuf {
        self.dirname.join(filename)
    }

    fn add_new_dataset(&mut self, dataset: OnDiskDataset) {
        self.datasets.push(dataset)
    }
}

impl DataIndex {
    fn query(&self, trigram: TriGram) -> Vec<FileId> {
        let raw_data: &[u8] = unsafe { self.mmap.as_slice() };
        let run_data = &raw_data[16777216*4+16..];

        let pointer = self.run_offsets[trigram as usize] as usize;
        let next_pointer = self.run_offsets[(trigram+1) as usize] as usize;
        let raw_run = &run_data[pointer..next_pointer];

        read_compressed_run(&raw_run)
    }
}

fn compress_run(files: &Vec<FileId>) -> Vec<u8> {
    let mut prev = 0;
    let mut result = vec![];
    for fid in files {
        let mut diff = (fid + 1) - prev;
        while diff >= 0x80 {
            result.push((0x80 | (diff & 0x7F)) as u8);
            diff >>= 7
        }
        result.push(diff as u8);
        prev = fid + 1
    }
    result
}

fn merge_index(files: &Vec<MergeHelper>, writer: &mut BufWriter<std::fs::File>, types: IndexType) -> io::Result<()> {
    let mut cursors = Vec::from_iter(files.iter().map(|f| Cursor::new(f.bytes)));
    let int_type = types as u32;

    for c in cursors.iter_mut() {
        assert!(c.read_u32::<LittleEndian>()? == 0xCA7DA7A, "Merge failed: invalid magic in index");
        assert!(c.read_u32::<LittleEndian>()? == 4, "Merge failed: unknown format version");
        assert!(c.read_u32::<LittleEndian>()? == int_type, "Merge failed: index types doesn't match");
        assert!(c.read_u32::<LittleEndian>()? == 0, "Merge failed: reserved field not 0");
    }

    writer.write_u32::<LittleEndian>(0xCA7DA7A)?;
    writer.write_u32::<LittleEndian>(4)?;
    writer.write_u32::<LittleEndian>(int_type)?;
    writer.write_u32::<LittleEndian>(0)?;

    let mut input_sizes = vec![Vec::with_capacity(16777216); files.len()];
    let mut sizes = Vec::with_capacity(16777216);
    let run_table_size = 4 * 16777216;
    let mut total_written = 0 as i64;
    
    let progress = make_progress_bar("read sizes", files.len() as u64);
    for i in 0..files.len() {
        for _ in 0..16777216 {
            input_sizes[i].push(cursors[i].read_u32::<LittleEndian>()?);
        }
        progress.set_position(i as u64);
    }
    progress.finish();

    let progress = make_progress_bar("merge runs", 16777216 as u64);
    writer.seek(SeekFrom::Current(run_table_size))?;
    for ndx in 0..16777216 {
        let mut all = vec![];
        let mut base = 0;
        for i in 0..files.len() {
            let run_len = input_sizes[i][ndx];
            let mut run =  vec![0; run_len as usize];
            cursors[i].read_exact(&mut run)?;
            all.extend(read_compressed_run(&run).iter().map(|x| x + base));
            base += files[i].files.len() as u32;
        }
        let bytes = compress_run(&all);
        sizes.push(bytes.len() as u32);
        total_written += bytes.len() as i64;
        
        writer.write(&bytes)?;

        if ndx & 0xFFF == 0 {
            progress.set_position(ndx as u64);
        }
    };

    writer.seek(SeekFrom::Current(-total_written-run_table_size))?;
    for trigram in 0..16777216 {
        writer.write_u32::<LittleEndian>(sizes[trigram])?;
    }

    progress.finish();

    Ok(())
}

fn make_progress_bar(msg: &str, total_size: u64) -> ProgressBar {
    let pb = ProgressBar::new(total_size);
    pb.set_style(ProgressStyle::default_bar()
        .template("{msg:20}{spinner:.green} [{elapsed_precise}] [{bar:40.cyan/blue}] {pos}/{len} ({eta})")
        .progress_chars("#>-"));
    pb.set_message(msg);
    pb
}

struct MergeHelper<'a> {
    bytes: &'a [u8],
    files: &'a Vec<String>,
}

fn database_compact(db: &mut OnDiskDatabase) -> Result<(), Box<Error>> {
    let mut builder = db.allocate_dataset(&vec![]);

    {
        let mut indexes : BTreeMap<IndexType, Vec<MergeHelper>> = BTreeMap::new();

        for &ref dataset in &db.datasets {
            for elem in &dataset.elements {
                let entry = indexes.entry(elem.ntype).or_insert(vec![]);
                let bytes : &[u8] = unsafe { elem.mmap.as_slice() };
                entry.push(MergeHelper{
                    bytes: bytes,
                    files: &dataset.files
                })
            }
            builder.files.extend(dataset.files.clone());
        }

        for (ntype, ndxes) in indexes.iter() {
            let path = builder.filepath(get_index_name(*ntype));

            let file = File::create(path)?;
            let mut writer = BufWriter::new(file);

            merge_index(ndxes, &mut writer, *ntype)?;

            builder.index_refs.push(*ntype);
        }
    }

    let old_datasets: Vec<OnDiskDataset> = db.datasets.drain(..).collect();

    db.datasets.clear();
    db.datasets.push(builder.commit()?);

    save_database(&db)?;

    for dataset in old_datasets {
        purge_dataset(&db, dataset)?;
    }

    Ok(())
}

fn database_compact_total(db: &mut OnDiskDatabase) -> Result<(), Box<Error>> {
    database_compact(db)
}

fn database_smart_compact(db: &mut OnDiskDatabase) -> Result<(), Box<Error>> {
    database_compact(db)
}

fn purge_dataset(db: &OnDiskDatabase, dataset: OnDiskDataset) -> io::Result<()> {
    for elem in dataset.elements {
        let elem_file = db.full_name(&elem.filename);
        remove_file(&elem_file)?;    
    }

    let fileset_file = db.full_name(&dataset.files_filename);
    remove_file(&fileset_file)?;

    let dataset_file = db.full_name(&dataset.filename);
    remove_file(&dataset_file)?;

    Ok(())
}

#[derive(RustcDecodable, RustcEncodable)]
pub struct DatabaseData  {
    version: String,
    name: String,
    datasets: Vec<String>,
    allocated_datasets: u32,
}

#[derive(RustcDecodable, RustcEncodable)]
pub struct FilesetData  {
    files: String,
    elements: BTreeMap<String, String>,
}

struct IndexData {
    ntype: IndexType,
    index: Vec<Vec<u32>>,
}

impl IndexData {
    pub fn new(ndxtype: IndexType) -> IndexData {
        IndexData {
            ntype: ndxtype,
            index: vec![vec![]; 16777216],
        }
    }
}

fn load_database(dirname: &Path) -> Result<OnDiskDatabase, Box<Error>> {
    let path = dirname.join("main.ursa");
    let mut file = File::open(path)?;
    let mut raw_data = String::new();
    file.read_to_string(&mut raw_data)?;

    let data : DatabaseData = json::decode(raw_data.as_str())?;

    assert!(data.version == "1.0", "Unknown database version");

    let mut datasets = vec![];

    for dataset in data.datasets {
        datasets.push(load_dataset(dirname, &dataset)?);
    }

    Ok(OnDiskDatabase {
        allocated_datasets: data.allocated_datasets,
        dirname: dirname.to_owned(),
        name: data.name,
        datasets: datasets,
    })
}

fn load_dataset(dirname: &Path, filename: &String) -> Result<OnDiskDataset, Box<Error>> {
    let path = dirname.join(filename);
    let mut file = File::open(path)?;
    let mut raw_data = String::new();
    file.read_to_string(&mut raw_data)?;

    let data : FilesetData = json::decode(raw_data.as_str())?;

    let files_filename = data.files;
    let files = load_file_list(&dirname.join(&files_filename))?;
    let elements = data.elements.values().map(|v| load_index_data(dirname, &v)).collect::<Result<Vec<_>, _>>()?;

    Ok(OnDiskDataset {
        elements: elements,
        filename: filename.clone(),
        files_filename: files_filename,
        files: files,
    })
}

fn save_json<T>(path: &PathBuf, data: &T) -> Result<(), Box<Error>> where
        T: rustc_serialize::Encodable {
    let encoded = json::encode(&data)?;

    let mut file = File::create(path)?;
    file.write_all(encoded.as_bytes())?;

    Ok(())
}

fn save_file_list(path: &PathBuf, filelist: &Vec<String>) -> io::Result<()> {
    let file = File::create(path)?;
    let mut writer = BufWriter::new(file);
    for entry in filelist {
        writer.write(entry.as_bytes())?;
        writer.write("\n".as_bytes())?;
    }

    Ok(())
}

fn load_file_list(path: &PathBuf) -> io::Result<Vec<String>> {
    let file = File::open(path)?;
    let rdr = BufReader::new(&file);
    Ok(rdr.lines().collect::<Result<Vec<_>, _>>()?)
}

fn init_database<'a>(dirname: &Path, name: &str) -> OnDiskDatabase {
    OnDiskDatabase {
        allocated_datasets: 0,
        dirname: dirname.to_owned(),
        name: name.to_owned(),
        datasets: vec![],
    }
}

fn save_database(db: &OnDiskDatabase) -> Result<(), Box<Error>> {
    let datasets = db.datasets.iter().map(|x| x.filename.to_owned()).collect();

    let data = DatabaseData {
        version: "1.0".to_owned(),
        name: db.name.to_owned(),
        allocated_datasets: db.allocated_datasets,
        datasets: datasets,
    };

    let mut path = PathBuf::from(&db.dirname);
    path.push("main.ursa");

    save_json(&path, &data)
}

fn index_update(ndx: &mut IndexData, file: FileId, val: TriGram) {
    let vec = &mut ndx.index[val as usize];
    if vec.last().map(|x| *x != file).unwrap_or(true) {
        vec.push(file)
    }
}

fn get_token_func<'a>(ntype: IndexType) -> fn(&'a [u8]) -> Box<Iterator<Item = TriGram> + 'a> {
    match ntype {
        IndexType::NGram3 => get_trigrams,
        IndexType::Base64 => get_b64_tokens,
    }
}

fn index_file(index: &mut IndexData, bytes: &[u8], id: u32) {
    let token_func = get_token_func(index.ntype);

    for token in token_func(bytes) {
        index_update(index, id, token);
    }
}

fn dump_compressed_runs(writer: &mut BufWriter<std::fs::File>, index: &IndexData) -> io::Result<()> {
    let mut sizes = vec![];
    let mut total_written = 0 as i64;
    let run_table_size = 4 * 16777216;

    writer.seek(SeekFrom::Current(run_table_size))?;
    for trigram in 0..16777216 {
        let bytes = compress_run(&index.index[trigram]);
        sizes.push(bytes.len() as u32);
        writer.write(&bytes)?;
        total_written += bytes.len() as i64;
    }

    writer.seek(SeekFrom::Current(-total_written-run_table_size))?;
    for trigram in 0..16777216 {
        writer.write_u32::<LittleEndian>(sizes[trigram])?;
    }

    Ok(())
}

fn get_index_name(ntype: IndexType) -> &'static str {
    match ntype {
        IndexType::NGram3 => "ngram3",
        IndexType::Base64 => "base64",
    }
}

fn save_index_data(path: PathBuf, index: &IndexData) -> io::Result<()> {
    let buffer = File::create(&path)?;
    let mut writer = BufWriter::new(buffer);

    writer.write_u32::<LittleEndian>(0xCA7DA7A as u32)?;
    writer.write_u32::<LittleEndian>(4)?;
    writer.write_u32::<LittleEndian>(index.ntype as u32)?;
    writer.write_u32::<LittleEndian>(0)?;

    dump_compressed_runs(&mut writer, index)
}

fn int_to_index_type(int: u32) -> Option<IndexType> {
    match int {
        0 => Some(IndexType::NGram3),
        1 => Some(IndexType::Base64),
        _ => None,
    }
}

fn load_index_data<'a>(dirname: &Path, filename: &str) -> io::Result<DataIndex> {
    let path = dirname.join(filename);
    let mmap = Mmap::open_path(path, Protection::Read)?;
    let mut run_offsets = Vec::with_capacity(16777217);
    let ntype;

    {
        let bytes: &[u8] = unsafe { mmap.as_slice() };
        let mut cursor_base = Cursor::new(bytes);
        let cursor = cursor_base.by_ref();

        let magic = cursor.read_u32::<LittleEndian>()?;
        assert!(magic == 0xCA7DA7A, "Invalid magic: {}", magic);

        let format_version = cursor.read_u32::<LittleEndian>()?;
        assert!(format_version == 4, "Unknown file version: {}", format_version);

        ntype = int_to_index_type(cursor.read_u32::<LittleEndian>()?).unwrap();

        let reserved = cursor.read_u32::<LittleEndian>()?;
        assert!(reserved == 0, "Reserved dword is not 0");

        let mut run_offset = 0;

        let progress = make_progress_bar(format!("load {}", filename).as_str(), 16777216 as u64);
        for ndx in 0..16777216 {
            run_offsets.push(run_offset);
            let len = cursor.read_u32::<LittleEndian>()?;
            run_offset += len as usize;

            if ndx & 0xFFF == 0 {
                progress.set_position(ndx as u64);
            }
        }
        run_offsets.push(run_offset);
        progress.finish();
    }

    Ok(DataIndex {
        filename: filename.to_owned(),
        ntype: ntype,
        mmap: mmap,
        run_offsets: run_offsets,
    })
}

struct DatasetBuilder {
    setid: u32,
    fileid: u32,
    indexes: Vec<IndexData>,
    index_refs: Vec<IndexType>,
    files: Vec<String>,
    dirname: PathBuf,
    indexed_bytes: usize,
}

impl<'a> DatasetBuilder {
    fn index(&mut self, path: &str) {
        let file_mmap = match Mmap::open_path(path, Protection::Read) {
            Ok(map) => map,
            Err(_) => { return; }
        };

        let bytes: &[u8] = unsafe { file_mmap.as_slice() };

        for elm in self.indexes.iter_mut() {
            index_file(elm, bytes, self.fileid);
        }

        self.indexed_bytes += bytes.len();

        self.files.push(path.to_owned());
        self.fileid += 1;
    }

    fn commit(self) -> Result<OnDiskDataset, Box<Error>> {
        let files_path = self.filepath("files");
        save_file_list(&files_path, &self.files)?;

        let mut elements = vec![];
        let mut element_map = BTreeMap::<String, String>::new();

        for &ref elm in &self.indexes {
            let index_name = get_index_name(elm.ntype);

            let path = self.filepath(index_name);
            save_index_data(path.clone(), &elm)?;

            let filename = self.filename(index_name);
            elements.push(load_index_data(self.dirname.as_path(), &filename)?);
            element_map.insert(get_index_name(elm.ntype).to_owned(), filename);
        }

        for ndxref in &self.index_refs {
            let index_name = get_index_name(*ndxref);

            let filename = self.filename(index_name);
            elements.push(load_index_data(self.dirname.as_path(), &filename)?);
            element_map.insert(index_name.to_owned(), filename);
        }

        let files_filename = self.filename("files");
        let data = FilesetData {
            elements: element_map,
            files: files_filename.clone(),
        };

        let dataset_path = self.filepath("dataset");
        let dataset_name = self.filename("dataset");
        save_json(&dataset_path, &data)?;

        Ok(OnDiskDataset {
            elements: elements,
            filename: dataset_name,
            files_filename: files_filename,
            files: self.files,
        })
    }

    fn filepath(&self, prefx: &str) -> PathBuf {
        let filename = self.filename(prefx);

        let mut path = self.dirname.clone();
        path.push(filename);

        path
    }

    fn filename(&self, prefx: &str) -> String {
        format!("{}.{}.ursa", prefx.to_owned(), self.setid)
    }

    fn indexed_bytes_total(&self) -> usize {
        self.indexed_bytes * self.indexes.len()
    }
}

use std::collections::HashSet;
fn get_all_files(db: &OnDiskDatabase) -> HashSet<String> {
    let mut existing_files = HashSet::new();
    
    for dataset in &db.datasets {
        for file in &dataset.files {
            existing_files.insert(file.clone());
        }
    }

    existing_files
}

fn database_new_dataset(db: &mut OnDiskDatabase, root: &Path, types: &Vec<IndexType>) -> Result<(), Box<Error>> {
    // TODO - in far future - synchronization and stuff
    let mut builder = db.allocate_dataset(types);

    let existing_files = get_all_files(db);

    for candidate in WalkDir::new(root) {
        match candidate {
            Ok(entry) => {
                if entry.file_type().is_file() {
                    let os_path = entry.path();

                    if let Some(path) = os_path.to_str() {
                        if existing_files.contains(&String::from(path)) {
                            // silently ignore?
                        } else {
                            builder.index(path);

                            if builder.indexed_bytes_total() > 0x80000000 {
                                db.add_new_dataset(builder.commit()?);
                                
                                if db.datasets.len() > 0x10 {
                                    database_smart_compact(db)?
                                }

                                builder = db.allocate_dataset(types);
                            }
                        }
                    } else {
                        // inform that we have invalid unicode?
                    }
                }
            },
            Err(e) => {
                println!("{}", e)
            }
        }
    };

    db.add_new_dataset(builder.commit()?);

    Ok(())
}

fn dataset_add_index(dirname: PathBuf, dataset: &mut OnDiskDataset, ntype: IndexType) -> Result<(), Box<Error>> {
    let mut updater = IndexData::new(ntype);

    for (fid, fname) in dataset.files.iter().enumerate() {
        let file_mmap = Mmap::open_path(fname, Protection::Read)?;

        let bytes: &[u8] = unsafe { file_mmap.as_slice() };
        index_file(&mut updater, bytes, fid as u32);
    }

    let index_name = get_index_name(ntype);
    let filename = format!("{}.reindex.{}.ursa", index_name.to_owned(), dataset.filename);
    
    let mut path = dirname.clone();
    path.push(filename.clone());
    
    save_index_data(path, &updater)?;

    dataset.elements.push(load_index_data(dirname.as_path(), &filename)?);

    Ok(())
}

fn database_reindex_datasets(db: &mut OnDiskDatabase, types: &Vec<IndexType>) -> Result<(), Box<Error>> {
    for dataset in db.datasets.iter_mut() {
        for ntype in types {
            if !dataset.elements.iter().any(|el| el.ntype == *ntype) {
                dataset_add_index(db.dirname.clone(), dataset, *ntype)?;
            }

            let mut element_map = BTreeMap::<String, String>::new();
            for &ref elem in &dataset.elements {
                let index_name = get_index_name(elem.ntype);
                element_map.insert(index_name.to_owned(), elem.filename.clone());
            }

            let data = FilesetData {
                elements: element_map,
                files: dataset.files_filename.clone(),
            };

            let mut path = db.dirname.clone();
            path.push(dataset.filename.clone());
            save_json(&path, &data)?;
        }
    }

    Ok(())
}

fn result_from_vector(files: Vec<FileId>) -> PartialResult {
    PartialResult::Selection(files)
}

fn vector_intersect<T>(left: &Vec<T>, right: &Vec<T>) -> Vec<T> where
        T: PartialOrd+Clone {
    let mut result = vec![];
    let mut li = 0;
    let mut ri = 0;
    while li < left.len() && ri < right.len() {
        if left[li] < right[ri] { li += 1; }
        else if left[li] > right[ri] { ri += 1; }
        else {
            result.push(left[li].clone());
            li += 1;
            ri += 1;
        }
    }
    result
}

fn vector_union<T>(left: &Vec<T>, right: &Vec<T>) -> Vec<T> where
        T: PartialOrd+Clone {
    let mut result = vec![];
    let mut li = 0;
    let mut ri = 0;
    while li < left.len() && ri < right.len() {
        if left[li] < right[ri] {
            result.push(left[li].clone());
            li += 1; 
        }
        else if left[li] > right[ri] {
            result.push(right[ri].clone());
            ri += 1;
        }
        else {
            result.push(left[li].clone());
            li += 1;
            ri += 1;
        }
    }
    result.extend_from_slice(&left[li..]); 
    result.extend_from_slice(&right[ri..]);
    result
}

fn intersect_results(left: &PartialResult, right: &PartialResult) -> PartialResult {
    match (left, right) {
        (&PartialResult::Everything, &PartialResult::Everything) => PartialResult::Everything,
        (&PartialResult::Everything, &PartialResult::Selection(ref part)) => result_from_vector(part.clone()),
        (&PartialResult::Selection(ref part), &PartialResult::Everything) => result_from_vector(part.clone()),
        (&PartialResult::Selection(ref l), &PartialResult::Selection(ref r)) => result_from_vector(vector_intersect(&l, &r)),
    }
}

fn sum_results(left: &PartialResult, right: &PartialResult) -> PartialResult {
    match (left, right) {
        (&PartialResult::Everything, _) => PartialResult::Everything,
        (_, &PartialResult::Everything) => PartialResult::Everything,
        (&PartialResult::Selection(ref l), &PartialResult::Selection(ref r)) => result_from_vector(vector_union(&l, &r)),
    }
}

fn query_index_internal<'a>(index: &'a DataIndex, query: &QueryPlan) -> PartialResult {
    match query {
        &QueryPlan::Primitive(trigram) => PartialResult::Selection(index.query(trigram)),
        &QueryPlan::Intersection(ref parts) => parts.iter().map(|p| query_index_internal(index, p)).fold(PartialResult::Everything, |a, b| { intersect_results(&a, &b) }),
        &QueryPlan::Union(ref parts) => parts.iter().map(|p| query_index_internal(index, p)).fold(PartialResult::Selection(vec![]), |a, b| { sum_results(&a, &b) }),
    }
}

fn make_plan(index: &DataIndex, query: &Query) -> QueryPlan {
    match query {
        &Query::Intersection(ref parts) => QueryPlan::Intersection(parts.iter().map(|p| make_plan(index, p)).collect()),
        &Query::Union(ref parts) => QueryPlan::Union(parts.iter().map(|p| make_plan(index, p)).collect()),
        &Query::Exact(ref text) => QueryPlan::Intersection(get_token_func(index.ntype)(text).map(|t| QueryPlan::Primitive(t)).collect()),
    }
}

fn query_internal<'a>(dataset: &'a OnDiskDataset, query: &Query) -> PartialResult {
    let mut partial = PartialResult::Everything;

    for &ref index in &dataset.elements {
        let plan = make_plan(index, query);
        partial = intersect_results(&partial, &query_index_internal(&index, &plan));
    }

    partial
}

fn do_query<'a>(index: &'a OnDiskDatabase, trigrams: &Query) -> Vec<String> {
    let mut names = vec![];
    for &ref dataset in &index.datasets {
        match query_internal(&dataset, trigrams) {
            PartialResult::Selection(p) => names.extend(p.iter().map(|el| dataset.get_file_name(*el).clone())),
            PartialResult::Everything => names.extend(dataset.files.clone()),
        }
    }

    names
}

fn init_new_database(name: &str) -> Result<(), Box<Error>> {
    let path = Path::new(name);

    if path.exists() {
        return Err(Box::new(RuntimeError::new("Database already exists")));
    }

    create_dir(&path)?;

    let database = init_database(&path, name);

    save_database(&database)
}

fn handle_client(db: &mut OnDiskDatabase, stream: &mut TcpStream) -> Result<(), Box<Error>> {
    let msg_len = stream.read_u32::<LittleEndian>()? as usize;
    let mut message = vec![0u8; msg_len];
    stream.read_exact(&mut message)?;

    let out = if let Some(command) = parse_query(&message) {
        match command {
            Command::DoQuery(query) => {
                let results = do_query(db, &query);

                let mut out = String::new();
                for r in results {
                    out += r.as_str();
                    out += "\n";
                }
                out
            },
            Command::DoIndex(root) => {
                let path = Path::new(&root);
                let types = vec![IndexType::NGram3];//, IndexType::Base64];
                database_new_dataset(db, path, &types)?;
                save_database(db)?;

                String::from("ok!")
            },
            Command::DoReindex() => {
                let types = vec![IndexType::NGram3, IndexType::Base64];
                database_reindex_datasets(db, &types)?;
                save_database(db)?;

                String::from("ok!")
            },
            Command::DoCompact() => {
                database_compact_total(db)?;

                String::from("ok!")
            },
            Command::DoPing() => {
                String::from("pong")
            }
        }
    } else {
        String::from("cannot parse query")
    };
    stream.write_u32::<LittleEndian>(out.len() as u32)?;
    stream.write(out.as_bytes())?;

	Ok(())
}

fn engine_serve(db: &mut OnDiskDatabase) -> Result<(), Box<Error>> {
    let listener = TcpListener::bind("0.0.0.0:9281").unwrap();

    for stream in listener.incoming() {
		match stream {
			Ok(mut stream) => {
				handle_client(db, &mut stream)?;
			}
			Err(_) => { /* connection failed */ }
		}
    }
    Ok(())
}

fn shell_main() -> Result<(), Box<Error>> {
    let matches = App::new("Ursa DB")
        .version("1.0")
        .settings(&[AppSettings::SubcommandRequiredElseHelp])
        .author("msm <msm@tailcall.pl>")
        .subcommand(SubCommand::with_name("serve")
            .about("Start server with given index")
            .arg(Arg::with_name("index")
                .multiple(true)
                .help("Index files to be used")
                .required(true)))
        .subcommand(SubCommand::with_name("create")
            .about("Create new database")
            .arg(Arg::with_name("name")
                .help("Database name")
                .required(true)))
        .get_matches();

    if let Some(matches) = matches.subcommand_matches("serve") {
        let mut db = load_database(Path::new(matches.value_of("index").unwrap()))?;
        engine_serve(&mut db)?;
    } else if let Some(matches) = matches.subcommand_matches("create") {
        init_new_database(matches.value_of("name").unwrap())?;
    }

    Ok(())
}

fn main() {
    match shell_main() {
        Ok(_) => {},
        Err(e) => {
            println!("{}", e);
        }
    }
}
